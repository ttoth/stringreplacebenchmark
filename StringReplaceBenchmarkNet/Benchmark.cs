﻿using BenchmarkDotNet.Attributes;
using System.Collections.Frozen;
using System.Text;
using System.Text.RegularExpressions;


namespace StringReplaceBenchmarkNet;

[MemoryDiagnoser]
public partial class Benchmark
{
    const int Iterations = 1_000;
    private readonly string _pattern = BuildPattern("Hello", "Bye", Iterations);
    private readonly FrozenDictionary<int, string> _replacements = BuildReplacements(Iterations).ToFrozenDictionary();

    [Benchmark]
    public void RegexReplace()
    {
        var result = MyRegex().Replace(_pattern, (match) => _replacements[int.Parse(match.Groups[1].Value)]);
    }

    [Benchmark]
    public void StringReplace()
    {
        var result = _pattern;

        for (int i = 0; i < Iterations; i++)
        {
            result = result.Replace($"@{i}@", _replacements[i], StringComparison.Ordinal);
        }
    }

    [Benchmark]
    public void StringBuilder()
    {
        var resultBuilder = new StringBuilder(_pattern);

        for (int i = 0; i < Iterations; i++)
        {
            resultBuilder = resultBuilder.Replace($"@{i}@", _replacements[i]);
        }

        var result = resultBuilder.ToString();
    }

    internal static string BuildPattern(string prefix, string iterationPrefix, int iterations)
    {
        var builder = new StringBuilder(prefix);

        for (int i = 0; i < iterations; i++)
        {
            builder.Append($"{iterationPrefix}@{i}@ ");
        }

        return builder.ToString();
    }

    internal static Dictionary<int, string> BuildReplacements(int iterations)
    {
        var result = new Dictionary<int, string>();

        for (int i = 0; i < iterations; i++)
        {
            result.Add(i, $"!!{i}!!");
        }

        return result;
    }

    [GeneratedRegex(@"@(\d+)@")]
    private static partial Regex MyRegex();
}
