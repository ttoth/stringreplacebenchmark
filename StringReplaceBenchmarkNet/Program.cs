﻿using BenchmarkDotNet.Running;
using StringReplaceBenchmarkNet;

BenchmarkRunner.Run<Benchmark>();

var benchmark = new Benchmark();
benchmark.StringReplace();
benchmark.StringBuilder();
benchmark.RegexReplace();